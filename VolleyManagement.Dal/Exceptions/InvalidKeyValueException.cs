﻿namespace VolleyManagement.Dal.Exceptions
{
    using System;

    /// <summary>
    /// Represents errors that occurs during the searching entity id
    /// in database
    /// </summary>
    public class InvalidKeyValueException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the InvalidKeyValueException
        /// class.
        /// </summary>
        public InvalidKeyValueException() :
            base("Specified key value does not exist in database")
        {
        }

        /// <summary>
        /// Initializes a new instance of the InvalidKeyValueException
        /// class with message and inner exception
        /// </summary>
        /// <param name="message">Message text</param>
        public InvalidKeyValueException(string message) :
            base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the InvalidKeyValueException
        /// class with message and inner exception
        /// </summary>
        /// <param name="message">Message text</param>
        /// <param name="innerException">Original exception</param>
        public InvalidKeyValueException(string message, Exception innerException) :
            base(message, innerException)
        {
        }
    }
}
