﻿namespace VolleyManagement.Services
{
    /// <summary>
    /// Container for constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Constant defined key for Entity's id
        /// </summary>
        public const string ENTITY_ID_KEY = "EntityId";
    }
}
